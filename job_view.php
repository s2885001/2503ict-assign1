<?PHP
/*
view of a particular job
*/
date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();

// details for job retrieved
try { 
  $query = $dbh->prepare("SELECT jobs.*, industries.name AS industry, employers.name AS employer FROM employers, industries, jobs WHERE 
                        jobs.id = :jobId AND jobs.employerID=employers.id AND employers.industryID=industries.id LIMIT 1");
  $query->bindValue(':jobId', (int)$_GET['id']);
  $query->execute();
  $row = $query->fetch();
  
} catch(PDOException $e) {
  pdo_error($e);
}

$smarty->assign('created', (int)$_GET['created']);
//$smarty->assign('updated', (int)$_GET['updated']);
$smarty->assign('job', $row);
$smarty->display('job_view.tpl');

unset($dbh); // close database
?>