<?PHP
/*
used for editing a particular job by employer
*/
date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();

 
// delete posted job

if(isset($_GET['delete']) && !empty($row['id']) ) { 
  try {
    $query = $dbh->prepare("DELETE FROM jobs WHERE id = :jobID LIMIT 1");
    $query->bindValue(':jobID', (int)$row['id']);
    $query->execute();

    header("Location: employer-jobs.php?deleted=".$row['id']."&employer=".$row['employerID']);
    exit("Job deleted! id=".$row['id']);
  } 
  catch(PDOException $e) {
    pdo_error($e);
  }
}

// retrieve details of jobs to populate form
try {
  $query = $dbh->prepare("SELECT jobs.*, industries.name AS industry FROM employers, industries, jobs WHERE 
                        jobs.id = :jobId AND jobs.employerID=employers.id AND employers.industryID=industries.id LIMIT 1");
  $query->bindValue(':jobId', (int)$_GET['id']);
  $query->execute();
  $row = $query->fetch();
  
} catch(PDOException $e) {
  pdo_error($e);
}



$smarty->assign('job', $row);
$smarty->display('job_edit.tpl');

unset($dbh); // close database
?>