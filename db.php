<?PHP
/*
   database funcitions used by application
*/

function db_open() {
/*
    Opens connection to database file.
*/
  try {
    $db = new PDO('sqlite:db/database.sqlite');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    die("ERROR! PDO: " . $e->getMessage());
  }
  
  return $db;
}

function pdo_error($e) {
/*
    catch PDO errors and return error.
*/
  die("<b>PDO Error:</b> " . $e->getMessage() . ' <br><b>Trace:</b> '  . $e->getTraceAsString() );
}

function sanitize($input) {
/*
    sanitizes user input provided.
*/
  return htmlentities(strip_tags($input) );
}

function searchJobs($jobIndustry = '', $jobSalary = '', $jobTerm = '') {
/*
    Searches database on given parameters.
      jobIndustry - Industry to search
      jobSalary - salary value
      jobTerm - keyword to search
*/
  $dbh = db_open();
  try {
    $sql = "SELECT DISTINCT jobs.*, employers.name AS employer, industries.name AS industryName FROM employers, industries, jobs WHERE 
          (jobs.employerID=employers.id AND employers.industryID=industries.id)";
    
    if( (int)$jobIndustry > 0) $sql .= " AND employers.industryID = :searchIndustry ";
    if( (int)$jobSalary > 0) $sql .= " AND jobs.salary >= :searchSalary";
    
    if(!empty($jobTerm) ) {
      $sql .= " AND ( jobs.title LIKE :searchTerm";
      $sql .= " OR employers.name LIKE :searchTerm";
      $sql .= " OR jobs.location LIKE :searchTerm )";
      $sql .= " OR jobs.description LIKE :searchTerm";
    }
    $sql .= " ORDER BY jobs.id DESC";
    
    $query = $dbh->prepare($sql);
    
    if( (int)$jobSalary > 0) $query->bindValue(':searchSalary', (int)$jobSalary);
    if( (int)$jobIndustry > 0) $query->bindValue(':searchIndustry', (int)$jobIndustry );
    if(!empty($jobTerm) ) $query->bindValue(':searchTerm', '%'.$jobTerm.'%');
    $query->execute();
    return $query->fetchAll();
    
  } catch(PDOException $e) {
    pdo_error($e);
  }
}


?>