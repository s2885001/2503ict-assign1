<?PHP

date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;

$dbh = db_open();

$smarty->display("index.tpl");

?>