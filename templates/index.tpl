{extends file="layout.tpl"}
{block name="pageTitle"}Home{/block}
{block name="pageContent"}
                    <div class="home-img">
                    <img src="image/home.jpg" alt="homepage">
                    </div>
                 
                      <h2>Todays job listings!</h2>
                   
                    
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Full Time</div>
                                <ul>
                                    <li class="li-items">JAVA Programmer</li>
                                    <li class="li-items">Clothing Expert</li>
                                    <li class="li-items">Pizza Maker</li>
                                    <li class="li-items">Admin Assistant</li>
                                </ul>
                            </div>
                        </div>
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Part Time</div>
                                <ul>
                                    <li class="li-items">Pizza Maker</li>
                                    <li class="li-items">Admin Assistant</li>
                                    <li class="li-items">JAVA Programmer</li>
                                    <li class="li-items">Clothing Expert</li>
                                </ul>
                            </div>
                        </div>
                    <div class="col-sm-6 col-md-3">
                        <div>
                            <div class="li-heading">Casual</div>
                                <ul>
                                    <li class="li-items">Carpenter</li>
                                    <li class="li-items">Sport Physician</li>
                                    <li class="li-items">Concreter</li>
                                    <li class="li-items">Chef</li>
                                </ul>
                            </div>
                        </div>

{/block}