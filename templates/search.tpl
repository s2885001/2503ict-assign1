{extends file="layout.tpl"}
{block name="pageTitle"}Search for a Job{/block}
{block name="pageContent"}

  <h2 class="pageHeader">Job Search</h2>
  <fieldset class="searchSet">
    <form role="form" action="" method="post">
      <div class="form-group">
        <label for="searchTerm">Search term</label>
        <input type="text" class="form-control" id="searchTerm" name="searchTerm" placeholder="Enter a keyword to search for within job listings">
      </div>

      <label for="searchSalary">Salary</label>
      <div class="form-group input-group">
        <input type="number" min="0" class="form-control" id="searchSalary" name="searchSalary" placeholder="Lowest starting salary eg. 65,000" value="{$smarty.post.searchSalary}">
        <span class="input-group-addon">per yr</span>
      </div>

      <label for="searchIndustry">Industry</label>
      <div class="form-group input-group">
        <select class="form-control" id="searchIndustry" name="searchIndustry">
          <option value=>All industries</option>
          {foreach $industries as $industry}
            <option value="{$industry.id}">{$industry.name}</option>
          {/foreach}
        </select>
      </div>
    
      <button type="submit" class="btn btn-default btn-success" name="searchSubmit">Search</button>
      <button type="reset" class="btn btn-default btn-warning">Reset</button>
    </form>
  </fieldset>

  {if isset($searchResults) }
    <hr>
    <h2 class="pageHeader">Search Results</h2>

    <table class="table table-hover">
      <thead>
        <tr>
          <th>Title</th><th>Salary</th><th>Location</th><th>Industry</th><th>Employer</th>
        </tr>
      </thead>
      <tbody>
        {if count($searchResults) > 0}
          {foreach $searchResults as $job}
            <tr>
              <td><a href="job_view.php?id={$job.id}"><span class="icon-arrow hidden-xs"></span>{$job.title}</a></td>
              <td>${$job.salary}</td>
              <td>{$job.location}</td>
              <td>{$job.industryName}</td>
              <td>{$job.employer}</td>
            </tr>
          {/foreach}
        {else}
          <tr>
            <td colspan="5">No results found!</td>
          </tr>
        {/if}
      </tbody>
    </table>
  {/if} 

{/block}