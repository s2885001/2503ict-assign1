<?php /* Smarty version Smarty-3.1.16, created on 2014-04-30 00:34:30
         compiled from "./templates/employer_job_view.tpl" */ ?>
<?php /*%%SmartyHeaderCode:611349399535f2c618e4548-26465230%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9beb8d84e531fd07fc38c401f4d42733eb0ace6e' => 
    array (
      0 => './templates/employer_job_view.tpl',
      1 => 1398782042,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398777505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '611349399535f2c618e4548-26465230',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535f2c61a21888_83206140',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535f2c61a21888_83206140')) {function content_535f2c61a21888_83206140($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Employers Job Listing</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          


    <h2 class="pageHeader"><?php echo $_smarty_tpl->tpl_vars['employerJobs']->value[0]['employer'];?>
's Job Listings</h2>
    <span class="hidden-xs">Please click a job listing below to edit or select advertise to create a new listing.</span>
    <a class="popupBtn pull-right" href="advertise.php?employer=<?php echo $_GET['employer'];?>
"><span class="icon-resume hidden-xs"></span>Advertise Job</a>
    <br>
    <br>
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Title</th><th>Salary</th><th>Location</th><th>Industry</th>
        </tr>
      </thead>
      <tbody>
        <?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employerJobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
          <tr>
            <td><a href="job_edit.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><span class="icon-arrow hidden-xs"></span><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</a></td>
            <td>$<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['job']->value['industryName'];?>
</td>
          </tr>
        <?php } ?>
      </tbody>
    </table>



        </div>
      </div>
    </div>
  </body>
  <div id="footer">
  <div class="container">
      	<div class="row text-center">
          <br><br><br>
          <p><b>This project has been completed by Daniel Cousins s2885001.</b></p>
           <a href="docs/doc.html" title="Documentation">Click here to view documentation for application.</a>
		</div>
  </div>
</div>
</html><?php }} ?>
