<?php /* Smarty version Smarty-3.1.16, created on 2014-04-30 01:01:42
         compiled from "./templates/job_edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:949327186535f2b6548f463-28950576%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '19d9488db5f2b6ac9bcb8c9b266e478881747180' => 
    array (
      0 => './templates/job_edit.tpl',
      1 => 1398783700,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398777505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '949327186535f2b6548f463-28950576',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535f2b655cb3e4_99764917',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535f2b655cb3e4_99764917')) {function content_535f2b655cb3e4_99764917($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Advertise Job</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          

  <?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
    <div class="alert alert-danger">
      <h4><?php echo $_smarty_tpl->tpl_vars['error']->value;?>
</h4>
    </div>
  <?php }?>

  
  <div class="modal fade" id="jobDelete" tabindex="-1" role="dialog" aria-labelledby="jobDeleteLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="jobDeleteModalLabel">Are you sure you want to delete?</h4>
        </div>
        
        <div class="modal-body">
          <p class="warning text-danger">Are you sure you want to delete this job listing permanently?</p>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <a class="btn btn-danger pull-left" href="job_edit.php?id=<?php echo $_GET['id'];?>
&amp;delete=true">Delete</a>
        </div>
      </div>
    </div>
  </div>

  <h2 class="pageHeader">Edit Job Details</h2>
  <fieldset class="">
    <form role="form" action="" method="post">
      <div class="form-group">
        <label for="jobTitle">Job Title</label>
        <input type="text" class="form-control" id="jobTitle" name="jobTitle" placeholder="Job advert title" required value="<?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
">
      </div>

      <label for="jobSalary">Salary</label>
      <div class="form-group input-group">
        <input type="number" min="0" class="form-control" id="jobSalary" name="jobSalary" placeholder="Advertised Salary" required value="<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
">
        <span class="input-group-addon">per yr</span>
      </div>

      <div class="form-group">
        <label for="jobLocation">Location</label>
        <input type="text" class="form-control" id="jobLocation" name="jobLocation" placeholder="Job Location" required value="<?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
">
      </div>

      <label for="jobDescription">Job Description</label>
      <div class="form-group">
        <textarea class="form-control" id="jobDescription" name="jobDescription" rows="6" maxlength="1500" placeholder="Description of job advertised:" required><?php echo $_smarty_tpl->tpl_vars['job']->value['description'];?>
</textarea>
      </div>

      <button type="submit" class="btn btn-default btn-success" name="jobSubmit">Save</button>
      <?php if (isset($_smarty_tpl->tpl_vars['error']->value)) {?>
        <a class="btn btn-default btn-warning" href="employer_jobs.php?employer=<?php echo $_smarty_tpl->tpl_vars['job']->value['employerID'];?>
">Cancel</a>
      <?php } else { ?>
        <button type="button" class="btn btn-default btn-warning" onclick="history.go(-1);">Cancel</button>
      <?php }?>
      <button type="button" class="btn btn-default btn-danger pull-right" data-toggle="modal" data-target="#jobDelete">Delete</button>
      
    </form>
  </fieldset>

        </div>
      </div>
    </div>
  </body>
  <div id="footer">
  <div class="container">
      	<div class="row text-center">
          <br><br><br>
          <p><b>This project has been completed by Daniel Cousins s2885001.</b></p>
           <a href="docs/doc.html" title="Documentation">Click here to view documentation for application.</a>
		</div>
  </div>
</div>
</html><?php }} ?>
