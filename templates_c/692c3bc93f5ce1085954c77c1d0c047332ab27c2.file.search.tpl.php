<?php /* Smarty version Smarty-3.1.16, created on 2014-04-30 00:50:51
         compiled from "./templates/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13110638535e636c7bb783-48931371%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '692c3bc93f5ce1085954c77c1d0c047332ab27c2' => 
    array (
      0 => './templates/search.tpl',
      1 => 1398783029,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398777505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13110638535e636c7bb783-48931371',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535e636c9e1390_88514179',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535e636c9e1390_88514179')) {function content_535e636c9e1390_88514179($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Search for a Job</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          

  <h2 class="pageHeader">Job Search</h2>
  <fieldset class="searchSet">
    <form role="form" action="" method="post">
      <div class="form-group">
        <label for="searchTerm">Search term</label>
        <input type="text" class="form-control" id="searchTerm" name="searchTerm" placeholder="Enter a keyword to search for within job listings">
      </div>

      <label for="searchSalary">Salary</label>
      <div class="form-group input-group">
        <input type="number" min="0" class="form-control" id="searchSalary" name="searchSalary" placeholder="Lowest starting salary eg. 65,000" value="<?php echo $_POST['searchSalary'];?>
">
        <span class="input-group-addon">per yr</span>
      </div>

      <label for="searchIndustry">Industry</label>
      <div class="form-group input-group">
        <select class="form-control" id="searchIndustry" name="searchIndustry">
          <option value=>All industries</option>
          <?php  $_smarty_tpl->tpl_vars['industry'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['industry']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['industries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['industry']->key => $_smarty_tpl->tpl_vars['industry']->value) {
$_smarty_tpl->tpl_vars['industry']->_loop = true;
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['industry']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['industry']->value['name'];?>
</option>
          <?php } ?>
        </select>
      </div>
    
      <button type="submit" class="btn btn-default btn-success" name="searchSubmit">Search</button>
      <button type="reset" class="btn btn-default btn-warning">Reset</button>
    </form>
  </fieldset>

  <?php if (isset($_smarty_tpl->tpl_vars['searchResults']->value)) {?>
    <hr>
    <h2 class="pageHeader">Search Results</h2>

    <table class="table table-hover">
      <thead>
        <tr>
          <th>Title</th><th>Salary</th><th>Location</th><th>Industry</th><th>Employer</th>
        </tr>
      </thead>
      <tbody>
        <?php if (count($_smarty_tpl->tpl_vars['searchResults']->value)>0) {?>
          <?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['searchResults']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
            <tr>
              <td><a href="job_view.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><span class="icon-arrow hidden-xs"></span><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</a></td>
              <td>$<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['job']->value['industryName'];?>
</td>
              <td><?php echo $_smarty_tpl->tpl_vars['job']->value['employer'];?>
</td>
            </tr>
          <?php } ?>
        <?php } else { ?>
          <tr>
            <td colspan="5">No results found!</td>
          </tr>
        <?php }?>
      </tbody>
    </table>
  <?php }?> 


        </div>
      </div>
    </div>
  </body>
  <div id="footer">
  <div class="container">
      	<div class="row text-center">
          <br><br><br>
          <p><b>This project has been completed by Daniel Cousins s2885001.</b></p>
           <a href="docs/doc.html" title="Documentation">Click here to view documentation for application.</a>
		</div>
  </div>
</div>
</html><?php }} ?>
