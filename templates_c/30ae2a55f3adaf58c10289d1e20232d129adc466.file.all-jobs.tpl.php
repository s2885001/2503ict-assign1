<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 14:21:58
         compiled from "./templates/all-jobs.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1136308759535e617b440ab9-37887125%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '30ae2a55f3adaf58c10289d1e20232d129adc466' => 
    array (
      0 => './templates/all-jobs.tpl',
      1 => 1398693173,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398745304,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1136308759535e617b440ab9-37887125',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535e617b5bef10_09814440',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535e617b5bef10_09814440')) {function content_535e617b5bef10_09814440($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jobs Listing</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
<!--            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li> -->
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          

  <h2 class="pageHeader">All Jobs</h2>
  <span>Below is a listing of all available jobs by all employers registered with CareerChoice.</span>
  <a class="popupBtn pull-right" href="search.php"><span class="icon-search hidden-xs"></span>Search Jobs</a>

  <table class="table table-hover">
    <thead>
      <tr>
        <th>Title</th><th>Salary <small class="hidden-xs">(p/a)</small></th><th>Location</th><th>Industry</th><th>Employer</th>
      </tr>
    </thead>
    <tbody>
      <?php  $_smarty_tpl->tpl_vars['job'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['job']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['allJobs']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['job']->key => $_smarty_tpl->tpl_vars['job']->value) {
$_smarty_tpl->tpl_vars['job']->_loop = true;
?>
        <tr>
          <td><a href="view-job.php?id=<?php echo $_smarty_tpl->tpl_vars['job']->value['id'];?>
"><span class="icon-arrow hidden-xs"></span><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</a></td>
          <td>$<?php echo $_smarty_tpl->tpl_vars['job']->value['salary'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['job']->value['industryName'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['job']->value['employer'];?>
</td>
        </tr>
        <?php } ?>
    </tbody>
  </table>

        </div>
      </div>
    </div>
  </body>
</html><?php }} ?>
