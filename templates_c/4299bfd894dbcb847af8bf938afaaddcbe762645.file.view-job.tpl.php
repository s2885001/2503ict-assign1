<?php /* Smarty version Smarty-3.1.16, created on 2014-04-29 23:42:27
         compiled from "./templates/view-job.tpl" */ ?>
<?php /*%%SmartyHeaderCode:359601874535e705fdba9e9-51044806%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4299bfd894dbcb847af8bf938afaaddcbe762645' => 
    array (
      0 => './templates/view-job.tpl',
      1 => 1398736988,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398777505,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '359601874535e705fdba9e9-51044806',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_535e705fea5b02_54843828',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_535e705fea5b02_54843828')) {function content_535e705fea5b02_54843828($_smarty_tpl) {?><!DOCTYPE html>
<html lang="en">

    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View Job</title>
   
    <link rel="stylesheet" href="css/style.css">
    <!-- Bootstrap -->
   <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->    
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    
  </head>
<body>
      <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Job Finder</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          </ul>
            <ul class="nav navbar-nav navbar-right">
            <li><a href="employers.php">Employers Home</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div id="main-body" class="container">
      <div class="row">
        <div class="col-sm-3">
          <div class="list-group">
            <a class="list-group-item" href="index.php">Home</a>
            <a class="list-group-item" href="search.php">Search for a job</a>
            <a class="list-group-item" href="job_list.php">List all jobs</a>
          </div>
        </div>
        <div class="col-sm-9">
          

  <?php if (!$_smarty_tpl->tpl_vars['job']->value['id']) {?>
    <h2 class="pageHeader">Job advert not found, please try again.</h2>
  <?php } else { ?>



    <h2 class="pageHeader">Job Details</h2>
    <fieldset class="">
      <form role="form">

        <div class="form-group">
          <label for="jobTitle">Title</label>
          <p class="form-control-static" id="jobTitle" name="jobTitle"><?php echo $_smarty_tpl->tpl_vars['job']->value['title'];?>
</p>
        </div>                   

        <label for="jobSalary">Salary</label>
        <div class="form-group input-group">
          <p class="form-control-static" id="jobSalary" name="jobSalary">$<?php echo number_format($_smarty_tpl->tpl_vars['job']->value['salary']);?>
</p>
          <span class="input-group-addon">per annum</span>
        </div>

        <div class="form-group">
          <label for="jobLocation">Location</label>
          <p class="form-control-static" id="jobLocation" name="jobLocation"><?php echo $_smarty_tpl->tpl_vars['job']->value['location'];?>
</p>
        </div>

        <div class="form-group">
          <label for="jobIndustry">Industry</label>
          <p class="form-control-static" id="jobIndustry" name="jobIndustry"><?php echo $_smarty_tpl->tpl_vars['job']->value['industry'];?>
</p>
        </div>

        <div class="form-group">
          <label for="jobEmployer">Employer</label>
          <p class="form-control-static" id="jobEmployer" name="jobTitle"><?php echo $_smarty_tpl->tpl_vars['job']->value['employer'];?>
</p>
        </div>

        <label for="jobDescription">Job Description</label>
        <div class="form-group">
          <p class="form-control-static" id="jobDescription" name="jobDescription"><?php echo nl2br($_smarty_tpl->tpl_vars['job']->value['description']);?>
</p>
        </div>
        
      </form>
    </fieldset>

  <?php }?>


        </div>
      </div>
    </div>
  </body>
  <div id="footer">
  <div class="container">
      	<div class="row text-center">
          <br><br><br>
          <p><b>This project has been completed by Daniel Cousins s2885001.</b></p>
           <a href="docs/doc.html" title="Documentation">Click here to view documentation for application.</a>
		</div>
  </div>
</div>
</html><?php }} ?>
