<?PHP
/*
 Used for creating new database entries by employers
*/
date_default_timezone_set("Australia/Brisbane");
include 'includes/Smarty/libs/Smarty.class.php';
require_once('db.php');

// Sanitizes variables and trim whitespace from string.
foreach($_POST as $k => $v) {
  $_POST[$k] = trim(sanitize($v) );
}

$smarty = new Smarty;
$dbh = db_open();


$smarty->assign('showPortal', true);
$smarty->assign('employerIndustry', (empty($row['name'])) ? 'unknown. Error!'.$_GET['employer'] : $row['name'] );
$smarty->display('advertise.tpl');

unset($dbh); // close database
?>